# Lexicon IT Konsult Big Programming Test #

## Goal of the exercise ##

The goal is for Lexicon IT Konsult to be able to evaluate your kowledge programming in a specific programming language.

## Your task ##

Your task is to write a simple but complete [Tic-tac-toe](http://en.wikipedia.org/wiki/Tic-tac-toe) game.

### About the game ###

The game of [Tic-tac-toe](http://en.wikipedia.org/wiki/Tic-tac-toe) should be familiar to just about everyone.

The game should be for one or two players.

### Requirements ###

* The game must be able to handle one or two human players.
* The program should be written in a language related to the position you are applying for. If you are applying for a position programming in C# then you should write this the code for this test in C#.
* The program should be written for the console, i.e. it should be completely text-based.
* The code needs to be well-commented, but not stupidly so. For example don't do e.g.

        // Assign the value of y to x
        x = y;

* The program needs to handle illegal or otherwise invalid input without unexpected results.
* If external libraries are used, buid-/install-instructions should be provided to allow Lexicon IT Konsult to be able to build or install the libraries.
* If external libraries are used, use free or open-source libraries.
* Build-instructions for the program needs to be included.

### Optional requirements ###

The following requirements are not mandatory, only implement one or more of them if you feel you have the time and knowledge.

* Zero-player mode, where the computer plays against itself.
* Use of modern features (e.g. C++11/C++14, Python 3, C# 5/.NET 4.5, or similar new revisions for the applicable languages) are recommended but not required.
* Saving a player-statistics dabase

#### Player statistics ####

This is an optional extra that you can design and implement if you feel you have the time. Exactly how you save the statisticts is up to you, it may be in a simple [CSV](http://en.wikipedia.org/wiki/Comma-separated_values) file, a [SQLite](https://www.sqlite.org/) database, or even using old [GDBM](http://www.gnu.org.ua/software/gdbm/) databse.

Statistics should include, but is not limited, to

* Player names
* Player wins using circles/crosses
* Player losses using circles/crosses
  
Statistics for the computer player should also be saved.

If you use an external library, please tell us what library you have used, including installation (or build) instructions for it.

## Getting started ##

Start by creating a user on https://bitbucket.org/, and create a fork of this "master" repository, this is so it will be easier to keep track of applicants and their progress.

When you are done, email us about it and provide a link to your fork.

## Other ##

There is no time-limits on this task, take the time you feel is needed to complete it.

The computer player doesn't have to be smart. In fact, it's okay if the comuter player is really stupid. The test is nt about how to make a winning computer player.

There are no requirements on coding-style, but please try to be both clean and descriptive. Also remember to indent the code as well as have some spacing between blocks.

Check in often, maybe not after every line or after fixin a simple spelling mistake, but its considered good to check in after every new "feature" or new "functionality", or after fixing bugs. You don't need to push to the `origin` repository after each checkin, but at least once every day when you're done for the day. Also try to not checkin something which don't build or doesn't work.

Tell us all special requirements are needed to build your program. This includes, but is not limited, to

* Integrated Development Environment used
* Build system used
* Operating system used
* Compiler/interpreter used, possibly including version if applicable
* Language selected

## Example session ##

This section contains an *example* session of a *hypotethical* solution. The game you make may have different output.

    Welcome to Lexicon IT Konsult Tic Tac Toe.

    Select number of human players:
    0 - Two computer players
    1 - One human and one computer player
    2 - Two human players
    > 1

    You have selected one human player versus a computer player.

    Player 1, please select rings or crosses:
    O - Select rings
    X - Select crosses
    > x

    Player 1 selected crosses.

    Randomly selecting who wil start... Crosses will start.

    First round
      | A | B | C |
    --+---+---+---+
    1 |   |   |   |
    --+---+---+---+
    2 |   |   |   |
    --+---+---+---+
    3 |   |   |   |
    --+---+---+---+

    Please select where to place your cross (e.g. "1A", "1B", "1C" etc.):
    > 2b

    Player 1 places a cross on 2B.
    Computer places a ring on 1A.

    Second round
      | A | B | C |
    --+---+---+---+
    1 | O |   |   |
    --+---+---+---+
    2 |   | X |   |
    --+---+---+---+
    3 |   |   |   |
    --+---+---+---+

    Please select where to place your cross (e.g. "1A", "1B", "1C" etc.):
    > 1C

    Player 1 places a cross on 1C.
    Computer places a ring on 2A.

    Third round
      | A | B | C |
    --+---+---+---+
    1 | O |   | X |
    --+---+---+---+
    2 | O | X |   |
    --+---+---+---+
    3 |   |   |   |
    --+---+---+---+

    Please select where to place your cross (e.g. "1A", "1B", "1C" etc.):
    > 3A

    Player 1 places a cross on 3A.

    Player one have won the game:

      | A | B | C |
    --+---+---+---+
    1 | O |   | X |
    --+---+---+---+
    2 | O | X |   |
    --+---+---+---+
    3 | X |   |   |
    --+---+---+---+

    Congratulations player 1!
